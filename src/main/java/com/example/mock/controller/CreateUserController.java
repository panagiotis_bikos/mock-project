package com.example.mock.controller;


import com.example.mock.entity.User;
import com.example.mock.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;



//                                          Test Controllers







@Controller
@RequestMapping("/users")
public class CreateUserController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/add")
    public @ResponseBody String addNewUser(@RequestParam String name,
                                           String email, String phonenumber,
                                           String password, String company){

        User n = new User();
        n.setName(name);
        n.setPhoneNumber(phonenumber);
        n.setEmail(email);
        n.setPassword(password);
        n.setCompany(company);
        userRepository.save(n);
        return "Saved";
    }

    @GetMapping("/all")
    public @ResponseBody List<User> getAllUsers(){
        return userRepository.findAll();
    }

//    @PostMapping("/delete/{email}")
//    public void deleteUser(@PathVariable String email){
//        userRepository.deleteByEmail(email);
//    }

}
