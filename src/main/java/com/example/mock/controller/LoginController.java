package com.example.mock.controller;

import com.example.mock.entity.User;
//import com.example.mock.model.LoginResponse;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping(value = "")
    public String showLoginPage(Model model){
//        SecurityContext contextHolder = SecurityContextHolder.getContext();
//        LoginResponse loginResponse = (LoginResponse) contextHolder.getAuthentication().getPrincipal();

        return "login";
    }

    @PostMapping(value = "")
    public String processLogin(@ModelAttribute("user") User user, Model model){

        return "redirect:/home";
    }



}
