package com.example.mock.controller;

import com.example.mock.entity.User;
import com.example.mock.form.RegisterForm;
import com.example.mock.mapper.RegisterFormToUser;
import com.example.mock.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private static final String USERS_ATTR = "user";

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private RegisterFormToUser mapper;

    @GetMapping(value = "")
    public String showRegisterPage(Model model){

        model.addAttribute(USERS_ATTR, new RegisterForm());
        return "register";
    }

    @PostMapping(value = "")
    public String submitRegisterForm(Model model,@ModelAttribute(USERS_ATTR) RegisterForm registerForm){

        User user =mapper.toUser(registerForm);
        userRepository.save(user);

        return "redirect:/home";}
}
