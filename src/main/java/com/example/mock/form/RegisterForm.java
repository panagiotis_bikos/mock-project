package com.example.mock.form;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class RegisterForm {

    private static final String PASS_PATTERN = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";

    @NotBlank(message = "Can't be empty")
    private String email;

    @NotBlank(message = "Can't be empty")
    @Pattern(regexp = PASS_PATTERN,message = "Not meeting the criteria")
    private String password;

    private String company;

    private String phoneNumber;

    @NotBlank(message = "Can't be empty")
    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
