package com.example.mock.mapper;

import com.example.mock.entity.User;
import com.example.mock.form.RegisterForm;
import org.springframework.stereotype.Component;

@Component
public class RegisterFormToUser {


    public User toUser(RegisterForm registerForm){

        User user = new User();
        user.setCompany(registerForm.getCompany());
        user.setPassword(registerForm.getPassword());
        user.setEmail(registerForm.getEmail());
        user.setName(registerForm.getName());
        user.setPhoneNumber(registerForm.getPhoneNumber());
        return user;

    }
}
