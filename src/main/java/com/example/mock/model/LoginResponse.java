//package com.example.mock.model;
//
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//
//import java.util.Collection;
//
//public class LoginResponse extends User {
//
//    private com.example.mock.entity.User user;
//
//    public LoginResponse(String username, String password, Collection<? extends GrantedAuthority> authorities, com.example.mock.entity.User user) {
//        super(username, password, authorities);
//        this.user = user;
//    }
//
//    public com.example.mock.entity.User getUser() {
//        return user;
//    }
//}
//
