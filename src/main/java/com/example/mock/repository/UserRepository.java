package com.example.mock.repository;

import com.example.mock.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

    User findByEmail(String email);

   // void deleteByEmail(String email);


}
